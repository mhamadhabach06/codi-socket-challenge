import React, { Component } from 'react';
import io from 'socket.io-client';

class App extends Component {

  state = {
    isConnected:false,
    id:null,
    ar:[],
    ar2:[]
  }
  socket = null

  componentWillMount(){

    this.socket = io('https://codi-server2.herokuapp.com');

    this.socket.on('connect', (newUser) => {
      this.setState({isConnected:true})
      
    })

    this.socket.on('pong!',()=>{
      console.log('the server answered!')
    })

    this.socket.on('peeps',(ids)=>{
      this.setState({ar:ids})
    })

    this.socket.on('new connection',(id)=>{
      this.state.ar.push(id);
      this.setState({ar:this.state.ar})
    })

    this.socket.on('new disconnection',(id)=>{
      this.setState({ar:this.state.ar.filter(idNew => idNew !== id)})
    })

    this.socket.on('pong!',(additionalStuff)=>{
      console.log('server answered!', additionalStuff)
    })

    this.socket.on('youare',(answer)=>{
      this.setState({id:answer.id})
    })

    this.socket.on('disconnect', () => {
      this.setState({isConnected:false})
    })

    this.socket.on('next',(message_from_server)=>{
      console.log(message_from_server)
    })

    /** this will be useful way, way later **/
    this.socket.on('room', old_messages => {

    // this.setState({old_messages:this.state.old_msg})
    console.log("old message: ",old_messages)
    this.setState({ar2:old_messages})
  })

  this.socket.on('room', new_messages => {

    // this.setState({old_messages:this.state.old_msg})
    console.log("new message: ",new_messages)
    this.setState({ar2:new_messages})
  })

    // this.socket.on('room_message', old_messages => console.log(old_messages))


  }

  componentWillUnmount(){
    this.socket.close()
    this.socket = null
  }
  

  render() {
    return (
      <div className="App">
        <div className='status-wrapper'>
            <div className='status'>status: {this.state.isConnected ? 'connected' : 'disconnected'}</div>
            <div className='id'>id: {this.state.id}</div>
        </div>
      {/* <button onClick={()=>this.socket.emit('ping!')}>ping</button> */}
      {/* <button onClick={()=>this.socket.emit('give me next')}>Next Message</button> */}
      {/* <button onClick={()=>this.socket.emit('addition')}>Addition</button> */}
      <button className='whoami' onClick={()=>this.socket.emit('whoami')}>Who am I?</button>
     <br></br><br></br> <div className='send-wrap'><input className='ans'></input>
      
      <button className='send' onClick={()=>this.socket.emit('message',{id:this.state.id,name:"Mohamad-Habach",text:document.querySelector(".ans").value})}>Send Answer</button>
      </div>
      <p className='active'>Active user: {this.state.ar.length}</p>
      <div className='msg'> {this.state.ar2.map(Data => {
                  return (

                    <div className='chat-msg'>
                        <span className='name' > {Data.name}:</span>

                        <span className='text'> {Data.text} </span><br></br>
                      </div>

                  );
                })} 
      </div><br></br>
      <div className='onlineuser'>{this.state.ar} </div>

      </div>
    );
  }
}

export default App;